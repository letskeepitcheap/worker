using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Serilog;

namespace Worker
{
    public class ExecuteTransactionsCommandHandler : IRequestHandler<ExecuteTransactionsCommand>
    {
        private ILogger _logger;
        private ILedgerRepository _repo;

        public ExecuteTransactionsCommandHandler(ILogger logger, ILedgerRepository repo)
        {
            _logger = logger;
            _repo = repo;
        }

        public async Task<Unit> Handle(ExecuteTransactionsCommand command, CancellationToken cancellation)
        {
            foreach (var entry in command.Entries)
            {
                _logger.Information($"Executing transaction {entry.Id} for user {entry.UserId}");

                await _repo.InsertLedgerEntryAsync(entry);
                await AddOrUpdateCategoryAsync(entry.Category);
            }
            return Unit.Value;
        }

        private async Task AddOrUpdateCategoryAsync(string category)
        {
            var categories = await _repo.GetCategoriesByCategoryAsync(category);
            if (!categories.Any())
            {
                await _repo.InsertLedgerEntryCategoryAsync(new LedgerEntryCategory()
                {
                    Category = category,
                    LastUsed = DateTime.Now,
                    CreatedDate = DateTime.Now
                });
            }
            else
            {
                var id = categories.First().Id;
                await _repo.UpdateCategoryLastUsedAsync(id, DateTime.Now);
            }
        }
    }
}