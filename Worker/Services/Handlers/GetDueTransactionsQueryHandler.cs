using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Serilog;

namespace Worker
{
    public class GetDueTransactionsQueryHandler : IRequestHandler<GetDueTransactionsQuery, IEnumerable<LedgerEntry>>
    {
        private const int MAXIMUM_USER_AGE_DAYS = 45;
        private ILogger _logger;
        private ILedgerRepository _repo;

        public GetDueTransactionsQueryHandler(ILogger logger, ILedgerRepository repo)
        {
            _logger = logger;
            _repo = repo;
        }

        public async Task<IEnumerable<LedgerEntry>> Handle(GetDueTransactionsQuery query, CancellationToken cancellation)
        {
            _logger.Information("Getting due transactions.");
            var frequencies = await _repo.GetFrequenciesAsync();

            var response = new List<LedgerEntry>();
            foreach (var frequency in frequencies)
            {
                var daysToLookBack = frequency.GetDaysToLookBack();
                var transactions = await _repo.GetRecurringTransactionsByFrequencyAndLastTriggeredAsync(frequency.Id, DateTime.Now.AddDays(-daysToLookBack));
                response.AddRange(from transaction in transactions
                                  select new LedgerEntry()
                                  {
                                      UserId = transaction.UserId,
                                      Category = transaction.Category,
                                      Description = transaction.Description,
                                      Amount = transaction.Amount,
                                      TransactionTypeId = transaction.TransactionTypeId,
                                      RecurringTransactionId = transaction.Id,
                                      TransactionDate = transaction.LastTriggered.AddDays(daysToLookBack),
                                      CreatedDate = DateTime.Now
                                  });
                foreach (var transaction in transactions)
                {
                    await _repo.UpdateRecurringTransactionLastTriggeredAsync(transaction.Id, transaction.LastTriggered.AddDays(daysToLookBack));
                }
                _logger.Information($"{frequency.Description}: {transactions.Count()}");
            }
            return response;
        }

    }
}