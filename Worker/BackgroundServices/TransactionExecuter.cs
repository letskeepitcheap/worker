using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using MediatR;
using Serilog;

namespace Worker
{
    public class TransactionExecuter : BackgroundService
    {
        private const int PERIOD_HOURS = 4;
        private const int PERIOD_MILLISECONDS = 1000 * 60 * 60 * PERIOD_HOURS;
        private ILogger _logger;
        private IMediator _mediatr;

        public TransactionExecuter(ILogger logger, IMediator mediatr)
        {
            _logger = logger;
            _mediatr = mediatr;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellation)
        {
            _logger.Information("TransactionExecuter beginning.");
            while (!cancellation.IsCancellationRequested)
            {
                var task = ExecuteTransactions()
                    .ContinueWith(task =>
                    {
                        _logger.Information("Executing transactions complete.");
                    }, TaskContinuationOptions.None)
                    .ContinueWith(task =>
                    {
                        _logger.Error(task.Exception.Message);
                    }, TaskContinuationOptions.OnlyOnFaulted);
                await Task.Delay(PERIOD_MILLISECONDS, cancellation);
            }
            _logger.Information("Terminating TransactionExecuter.");
        }

        private async Task ExecuteTransactions()
        {
            _logger.Information("Executing transactions starting.");
            IEnumerable<LedgerEntry> transactions = null;
            try
            {
                transactions = await _mediatr.Send(new GetDueTransactionsQuery());
            }
            catch (Exception e)
            {
                _logger.Information(e.Message);
                _logger.Information(e.StackTrace);
            }
            if (!transactions.Any())
            {
                _logger.Information("No transactions due.");
                return;
            }
            _logger.Information($"Found {transactions.Count()} transactions to execute.");
            var command = new ExecuteTransactionsCommand() { Entries = transactions };
            try
            {
                await _mediatr.Send(command);
            }
            catch (Exception e)
            {
                _logger.Information(e.Message);
                _logger.Information(e.StackTrace);
            }
        }
    }
}