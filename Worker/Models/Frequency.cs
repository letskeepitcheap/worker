using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Worker
{
    public class Frequency
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Description { get; set; }
        public int ApproxTimesPerYear { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }

        public int GetDaysToLookBack() => (int)Math.Round(ApproxTimesPerYear switch
        {
            52 => 7,                                                        // Weekly
            26 => 14,                                                       // Biweekly
            24 => GetSemimonthlyLookbackDays(),                             // Semimonthly                                                    
            12 => (DateTime.Now - DateTime.Now.AddMonths(-1)).TotalDays,    // Monthly
            4 => (DateTime.Now - DateTime.Now.AddMonths(-3)).TotalDays,     // Quarterly
            2 => (DateTime.Now - DateTime.Now.AddMonths(-6)).TotalDays,     // Semiannually
            1 => (DateTime.Now - DateTime.Now.AddYears(-1)).TotalDays,      // Annually
            _ => throw new ArgumentException($"Unknown frequency {Description}")
        });

        private double GetSemimonthlyLookbackDays()
        {
            var today = DateTime.Now.Day;
            var daysInMonth = DateTime.Now.AddMonths(1).AddDays(-today).Day;

            // If today is in the first half of the month, use half of the number of days in
            // the previous month, otherwise use half of the number of days in the current month.
            // Thus, if today is March 3, look back 14 days (number of days in February, usually)
            // to February 17; however, if today is March 25, look back 15.5 (16) days to
            // March 9.
            if (today < daysInMonth / 2)
            {
                return (DateTime.Now - DateTime.Now.AddMonths(-1)).TotalDays / 2;
            }
            else
            {
                return (DateTime.Now.AddMonths(1) - DateTime.Now).TotalDays / 2;
            }
        }
    }

}